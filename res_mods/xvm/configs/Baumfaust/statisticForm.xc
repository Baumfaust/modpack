﻿/**
 * Parameters of the Battle Statistics form.
 * Параметры окна статистики по клавише Tab.
 */
{
  "statisticForm": {
    // true - Enable display of battle tier.
    // true - включить отображение уровня боя.
    "showBattleTier": true,
    // true - Disable Platoon icons.
    // true - убрать отображение иконки взвода.
    "removeSquadIcon": false,
    // Display options for Team/Clan logos (see battleLoading.xc).
    // Параметры отображения иконки игрока/клана (см. battleLoading.xc).
    "clanIcon": {
      "show": true,
      "x": 0,
      "y": 6,
      "xr": 0,
      "yr": 6,
      "w": 16,
      "h": 16,
      "alpha": 90
    },
    // Display format for the left panel (macros allowed, see macros.txt).
    // Формат отображения для левой панели (допускаются макроподстановки, см. macros.txt).
    "formatLeftNick": "{{name%.15s~..}}",
    // Display format for the right panel (macros allowed, see macros.txt).
    // Формат отображения для правой панели (допускаются макроподстановки, см. macros.txt).
    "formatRightNick": "{{name%.15s~..}}",
    // Display format for the left panel (macros allowed, see macros.txt).
    // Формат отображения для левой панели (допускаются макроподстановки, см. macros.txt).
    "formatLeftVehicle": "{{vehicle}}<font face='Lucida Console' size='{{xvm-stat?12|0}}'> <font color='{{c:wn8}}'>{{wn8%4d|----}} </font> <font color='{{c:rating}}'>{{rating%2d~%|--%}} </font><font color='{{c:kb}}'>{{kb%2d~k|--k}}</font>  </font>",
    // Display format for the right panel (macros allowed, see macros.txt).
    // Формат отображения для правой панели (допускаются макроподстановки, см. macros.txt).
    "formatRightVehicle": "<font face='Lucida Console' size='{{xvm-stat?12|0}}'> <font color='{{c:wn8}}'>{{wn8%4d|----}} </font> <font color='{{c:rating}}'>{{rating%2d~%|--%}} </font><font color='{{c:kb}}'>{{kb%2d~k|--k}}</font>  </font>{{vehicle}}"
  }
}
